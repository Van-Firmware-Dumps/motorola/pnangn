#
# Copyright (C) 2025 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_pnangn.mk

COMMON_LUNCH_CHOICES := \
    lineage_pnangn-user \
    lineage_pnangn-userdebug \
    lineage_pnangn-eng
