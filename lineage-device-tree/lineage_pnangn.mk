#
# Copyright (C) 2025 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from pnangn device
$(call inherit-product, device/motorola/pnangn/device.mk)

PRODUCT_DEVICE := pnangn
PRODUCT_NAME := lineage_pnangn
PRODUCT_BRAND := motorola
PRODUCT_MODEL := moto g 5G - 2023
PRODUCT_MANUFACTURER := motorola

PRODUCT_GMS_CLIENTID_BASE := android-motorola

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="pnangn_g-user 14 U1TPNS34M.26-78-3-13 051b1b release-keys"

BUILD_FINGERPRINT := motorola/pnangn_g/pnangn:14/U1TPNS34M.26-78-3-13/051b1b:user/release-keys
